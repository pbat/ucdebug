
import random
import numpy as np

from otree.api import (
    models, widgets, BaseConstants, BaseSubsession, BaseGroup, BasePlayer,
    Currency as c, currency_range
)

class Constants(BaseConstants):
    name_in_url = 'ucpub'
    players_per_group = 3
    num_rounds = 30
    endowment = c(10)
    coefficient = 1.5
    forced_probability = .3

def _is_treatment(self):
    """
    To be used both in Subsession and in Player
    """
    return self.session.config['group_type'] == 'treatment'

class Subsession(BaseSubsession):
    @property
    def is_treatment(self):
        return _is_treatment(self)

    def creating_session(self):
        matrix = self.get_group_matrix()
        matrix = sorted([sorted(l) for l in matrix])
        new_matrix = [[] for l in matrix]

        # Pseudo-random but deterministic:

        for i in range(len(matrix)):
            for j in range(3):
                l_i = (i+j) % len(matrix)
                new_matrix[i].append(matrix[l_i][j])

        self.set_group_matrix(new_matrix)

class Group(BaseGroup):
    total_contribution = models.CurrencyField()
    # Returns from (augmented) common pool
    share = models.CurrencyField()



class Player(BasePlayer):
    # This is intended contribution:
    contribution = models.CurrencyField(label='How much do you want to contribute?', max=Constants.endowment, min=0,
        choices=[[c(i), i] for i in range(int(Constants.endowment) + 1)],
        widget=widgets.RadioSelectHorizontal)
    timed_out = models.BooleanField(initial=False)
