
from otree.api import Currency as c, currency_range
from ._builtin import Page, WaitPage
from .models import Constants


class Contribution(Page):
    form_model = 'player'
    form_fields = ['contribution']

    def is_displayed(self):
        max_rounds = self.session.config.get('max_rounds', -1)
        return (max_rounds == -1) or (self.round_number <= max_rounds)

    def vars_for_template(self):
        return dict(other_players=Constants.players_per_group - 1,
                    previous_rounds=range(1, self.round_number + 1),
                    players=self.group.get_players(),
                    past_contribs={},
                    pid = self.player.id_in_group,
                    round_number=self.round_number,
                    endow_int=int(Constants.endowment))

    def before_next_page(self):
        if self.timeout_happened:
            # "Force slowest user(s)" inserts zeros which are otherwise
            # undistinguishable from actual zeros:
            self.player.timed_out = True


page_sequence = [Contribution]
